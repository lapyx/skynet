<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/battle', ['as' => 'battle', 'uses' => 'BattleController@index']);
    Route::post('/battle', ['as' => 'battle.create', 'uses' => 'BattleController@create']);
    Route::get('/battle/wait_opponent', ['as' => 'battle.wait_opponent', 'uses' => 'BattleController@waitOpponent']);
    Route::get('/battle/process', ['as' => 'battle.process', 'uses' => 'BattleController@process']);
    Route::post('/battle/set_option', ['as' => 'battle.set_option', 'uses' => 'BattleController@setOption']);
    Route::any('/battle/finish', ['as' => 'battle.finish_test', 'uses' => 'BattleController@finishTest']);
    Route::get('/battle/result/{battle}', ['as' => 'battle.result', 'uses' => 'BattleController@result']);

    Route::get('/exam/{exam}', ['as' => 'exam', 'uses' => 'ExamController@index']);
    Route::post('/exam/{exam}', ['as' => 'exam.start', 'uses' => 'ExamController@startTest']);

    Route::get('/test/{userTest}', ['as' => 'exam.test', 'uses' => 'ExamController@test']);
    Route::post('/test/{userTest}', ['as' => 'exam.set_option', 'uses' => 'ExamController@setOption']);
    Route::post('/test/{userTest}/finish', ['as' => 'exam.finish_test', 'uses' => 'ExamController@finishTest']);

    Route::get('/profile/{user}',           ['as' => 'profile',         'uses' => 'HomeController@profile']);
    Route::get('/special',           ['as' => 'special',         'uses' => 'HomeController@special']);
});

Route::get('/', 'HomeController@index')->name('home');
