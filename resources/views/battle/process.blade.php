@php
    /** @var \App\Models\Battle $battle */
    $opponent = $battle->user_id == $user->id ? $battle->opponent : $battle->user;
@endphp
@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                У вас дуэль с {!! $opponent->link() !!}
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{ asset('i/img_avatar1.png') }}" width="150px" />
                        <div align="center">Вы</div>
                    </div>
                    <div class="col-md-8">
                        <div class="card card-default" align="left">
                            <div class="card-header">
                                <span class="count float-right">Пройдено: <span>{{ $count_results }}</span>/{{ $count_questions }}</span>
                            </div>
                            <div class="card-body">
                                @include('battle.question')
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <img src="{{ asset('i/img_avatar1.png') }}" width="150px" />
                        <div align="center">{{ $opponent->name }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('terminator')

@endsection
