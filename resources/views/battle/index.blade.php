@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Подача заявки на дуэль</div>

            <div class="card-body">
                <form action="{{ route('battle.create') }}" method="POST">
                    <div class="form-group">
                        <label for="predmet_id">Выбрать предмет:</label>
                        <select id="predmet_id" name="predmet_id" class="form-control">
                            @foreach(\App\Models\Predmet::all() as $one)
                                <option value="{{ $one->id }}">{{ $one->name }} ({{ $one->exam->name }})</option>
                            @endforeach
                        </select>
                    </div>
                    <button class="btn btn-primary">Поиск соперника</button>
                    <a class="btn btn-link" href="{{ route('home') }}">Отмена</a>
                </form>
            </div>
        </div>
    </div>
@endsection
