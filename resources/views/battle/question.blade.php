<?php
    /** @var \App\Models\Question $question */
    /** @var \App\Models\UserTest $test */
?>
<div class="card card-default" align="left">
    <div class="card-header">{{ $test->getIndexQuestion($question) }}. {{ $question->name }}</div>
    <div class="card-body">
        <form action="{{ route('battle.set_option', $test) }}" method="POST">
            @foreach($question->options as $option)
                <div class="form-group">
                    @php $checked = $test->checkOptionResult($option->question, $option) @endphp
                    <input {{ $checked ? 'checked' : '' }} id="option_{{ $option->id }}" type="radio" name="option" value="{{ $option->id }}">
                    <input type="hidden" name="start_time" value="{{ time() }}">
                    <label for="option_{{ $option->id }}">{{ $loop->index+1 }}. {{ $option->option }}</label>
                </div>
            @endforeach
            <button class="btn btn-sm btn-primary float-left">Ответить</button>
        </form>
    </div>
    <div class="card-footer">
        <div class="btn-group">
            @if ($test->hasPrev($question))
                <a href="{{ route('battle.process') }}?q={{ $index-1 }}" class="btn btn-sm btn-primary"><< Назад</a>
            @endif
            @if ($test->hasNext($question))
                <a href="{{ route('battle.process') }}?q={{ $index+1 }}" class="btn btn-sm btn-primary">Вперед >></a>
            @endif
        </div>
        <form action="{{ route('battle.finish_test') }}" method="POST" class="float-right">
            @if ($count_results >= $count_questions)
                <button class="btn btn-sm btn-primary float-right">Завершить тестирование</button>
            @else
                <button class="btn btn-sm btn-primary float-right">Я сдаюсь...</button>
            @endif
        </form>
    </div>
</div>