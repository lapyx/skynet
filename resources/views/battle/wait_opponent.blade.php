@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Ждём соперника...</div>

            <div class="card-body" align="center">
                <div class="spinner-border text-primary" style="width: 5rem;height: 5rem;margin-bottom: 10px;"></div>
                <h4>Идёт поиск соперника...</h4>
            </div>
        </div>
    </div>
    <script>
        $(function () {
           setTimeout(function () {
              location.href = '/battle/process'
           }, 4000);
        });
    </script>
@endsection
