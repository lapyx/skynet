@php
    /** @var \App\Models\UserTest $test */
    /** @var \App\Models\Question[] $questions */
    /** @var \App\Models\Battle $battle */
    $questions = $test->getQuestions();
@endphp
@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                Результаты тестирования {{ $test->created_at->format('d.m.Y H:i') }}
                <hr>
                <h4>Пройдено: {{ $test->results->count() }}/{{ $questions->count() }}</h4>
                <h4>Правильных ответов: {{ $test->countResultRightBattle($user) }}</h4>
                <h4>
                    @if (true || $battle->isProcess())
                        <span class="text-success">Поздравляем, вы победили!</span>
                    @endif
                </h4>
                <div class="card-body" align="left">
                    <ul class="list-group">
                        @foreach($questions as $question)
                            @php $result = $test->getResultQuestion($question, $user) @endphp
                                <li class="list-group-item">
                                    @if ($result)
                                        @if ($result->is_right)
                                            <i style="font-size: 130%;" class="fa fa-check-square-o text-success" data-toggle="tooltip" data-placement="top" title="Ответ верный"></i>
                                        @else
                                            <i style="font-size: 130%;" class="fa fa-close text-danger" data-toggle="tooltip" data-placement="top" title="Ответ неверный"></i>
                                        @endif
                                    @else
                                        <i style="font-size: 130%;" class="fa fa-square-o" data-toggle="tooltip" data-placement="top" title="Не указан ответ"></i>
                                    @endif
                                    {{ $question->name }}
                                </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
