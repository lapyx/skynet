<div id="terminator">
    <p class="speech"><i>Астанавился, бэйби??</i></p>
    <img src="{{ asset('i/terminator.png') }}" width="190px" />
</div>
<script>
    $(function () {
        setTimeout(function(){
            $('#terminator').slideDown();

            setTimeout(function(){
                $( "#terminator" ).animate({
                    height: "toggle"
                }, 4000);

                setTimeout(function(){
                    $('#terminator .speech').text('I\'ll be back...');
                },700);
            }, 2000);
        }, 3000);
    });
</script>