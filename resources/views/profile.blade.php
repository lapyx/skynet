@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header" style="text-align:center; margin-bottom: 0px;"><h4 class="card-title" style="margin-bottom: 0px;">Профиль</h4></div>
            <div class="row card-body">
                <div class="col-md-3 col-sm-12">
                    <img src="{{ asset('i/img_avatar1.png') }}" width="190px" />
                </div>
                <div class="col-md-9">
                    <div>
                        Имя: {{ $user->name }}
                    </div>
                    <div>
                        Пол: М
                    </div>
                    <div>
                        Возраст: 16
                    </div>
                    <div>
                        Пройдено тестов: {{ $all_tests_count }}
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 30px;">
            <div class="card-header" style="text-align:center;"><h4 class="card-title" style="margin-bottom: 0px;">Статистика ответов</h4></div>
            <div class="card-body" align="left">
                @php $i = 0; @endphp
                <table class="table">
                    <thead>
                        <tr>
                            <th>Тест №</th>
                            <th>Правильных/Всего</th>
                            <th>Процент успешности</th>
                            <th>Результат</th>
                            <th>Дата</th>
                            <th>Повторно</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($results as $result)
                            @php
                                $test = app(App\Models\UserTest::class)->find($result['test_id']);
                                $predmet = app(App\Models\Predmet::class)->find($test->predmet_id);
                            @endphp
                            <tr class="{{ $result['right_percent'] > 60 ? 'text-success' : 'text-danger' }}">
                                <td><a class="btn-link" href="{{ route('exam.test', $test) }}">{{ $predmet->name }}<a/></td>
                                <td>{{ $result['right_count'] }}/{{ $result['all_count'] }}</td>
                                <td>{{ $result['right_percent'] }}%</td>
                                <td>{{ $result['right_percent'] > 60 ? 'Пройден' : 'Не пройден' }}</td>
                                <td>{{ $test->created_at->format('d.h.Y') }}</td>
                                <td><a class="btn-link" href="{{ route('exam.start', $predmet->exam) }}">Пройти еще раз</a></span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
