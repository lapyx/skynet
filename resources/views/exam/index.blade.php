@php /** @var \App\Models\Exam $exam */ @endphp
@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Выберите предмет для тестирования</div>

            <div class="card-body" align="center">
                <form action="{{ route('exam.start', $exam)  }}" method="POST">
                    <div class="list-group col-sm-8 col-xs-12">
                        @foreach($exam->predmets as $one)
                            {{--<div class="form-group">--}}
                                <button name="predmet_id" value="{{ $one->id }}" class="list-group-item-info list-group-item btn btn-lg btn-primary">{{ $one->name }}</button>
                            {{--</div>--}}
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
