@php /** @var \App\Models\Exam $exam */ @endphp
@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">Тестирование началось
                <span class="count float-right">Пройдено: <span>{{ $count_results }}</span>/{{ $count_questions }}</span></div>

            <div class="card-body" align="center">
                <form action="" method="POST">
                    @include('exam.question', ['question' => $question])
                </form>
            </div>
        </div>
    </div>
</div>

@include('terminator')

@endsection
