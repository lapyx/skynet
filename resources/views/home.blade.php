@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Выберите экзамен</div>

            <div class="card-body" align="center">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @foreach(\App\Models\Exam::all() as $one)
                    <div class="list-group col-sm-8 col-xs-12">
                        <a href="{{ route('exam', $one) }}" class="list-group-item-info list-group-item  btn btn-lg btn-primary">{{ $one->title }} ({{ $one->name }})</a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
