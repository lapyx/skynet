<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserTest;
use App\Models\UserResult;

use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $test = $this->user->currentTest();
        if ($test) {
            return redirect()->route('exam.test', $test);
        }

        return view('home');
    }

    /**
     * profile page
     * @param User $user
     */
    public function profile(User $user) {
        $all_tests_count = UserTest::where('user_id', $user->id)->count();
        $results = UserResult::where('user_id', $user->id)->get()->groupBy('test_id')->toArray();
        foreach ($results as &$test) {
            $right_count = 0;
            $test_id = null;
            array_map(function ($element) use (&$right_count, &$test_id){
                if ($element['is_right']) $right_count += 1;
                $test_id = $element['test_id'];
            }, $test);
            $all_count = count($test);
            $test['right_count'] = $right_count;
            $test['all_count'] = $all_count;
            $test['right_percent'] = floor($right_count / $all_count * 100);
            $test['test_id'] = $test_id;
        }
        return view('profile', compact('user', 'all_tests_count', 'results'));
    }

    /**
     * @param User $user
     */
    public function special() {
        if (session()->has('special')) {
            session()->forget('special');
        } else {
            session()->put('special', true);
        }
        return back();
    }
}
