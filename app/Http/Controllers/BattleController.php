<?php

namespace App\Http\Controllers;

use App\Models\Battle;
use App\Models\Predmet;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\UserResult;
use App\Models\UserTest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class BattleController extends Controller
{
    public function index()
    {
        $battle = $this->user->currentBattle();
        if ($battle) {
            return redirect()->route('battle.wait_opponent');
        }

        return view('battle.index');
    }

    public function create()
    {
        $battle = $this->user->currentBattle();
        if ($battle) {
            return redirect()->route('battle.wait_opponent');
        }

        try {
            $predmet_id = \request('predmet_id');
            $predmet = Predmet::query()->findOrFail($predmet_id);

            if ($this->user->inBattle())
                throw new \Exception('Вы уже учавствуеете в дуэле!');

            //if (false || ($battle = Battle::findByCriteria($this->user, $predmet))){
                $ides = array_map(function($element) {
                    return $element['id'];
                }, app(QuestionController::class)->getQuestionsList($predmet, $this->user, 5));
                if (!$ides)
                    throw new Exception('У предмета еще не составлены вопросы');

                $test = new UserTest();
                $test->fill([
                    'predmet_id' => $predmet->id,
                    'questions' => $ides,
                ]);
                $test->save();
//
//                $battle->opponent_id = $this->user->id;
//                $battle->test_id = $test->id;
//                $battle->save();
//
//                return redirect()->route('battle.process');
//            }

            Battle::create([
                'test_id' => $test->id,
                'opponent_id' => 2,
                'exam_id' => $predmet->exam_id,
                'predmet_id' => $predmet->id,
                'user_id' => $this->user->id,
            ]);

            return redirect()->route('battle.wait_opponent');
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function waitOpponent()
    {
//        $battle = $this->user->currentBattle();
//        if (!$battle) {
//            return redirect()->route('battle');
//        }
//
//        if ($battle->isProcess()) {
//            return redirect()->route('battle.process');
//        }

        return view('battle.wait_opponent', [
            'time' => time()
        ]);
    }

    public function process()
    {
        $battle = $this->user->currentBattle();
        if (!$battle) {
            return redirect()->route('battle');
        }

        $test = $battle->test;
        $index = \request('q', 1)-1;
        if (array_key_exists($index, $test->questions) === false) {
            $index = 0;
        }
        $count_questions = count($test->questions);
        $count_results = $this->user->countTestResults($test);
        $question = $test->getQuestion($index);

        return view('battle.process', [
            'battle' => $battle,
            'test' => $test,
            'question' => $question,
            'count_questions' => $count_questions,
            'count_results' => $count_results,
            'index' => $index+1
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setOption()
    {
        $battle = $this->user->currentBattle();
        if (!$battle) {
            return redirect()->route('battle');
        }

        $test = $battle->test;

        $option = \request('option');
        try {
            if (!$option)
                throw new Exception('Выберите один из вариантов ответов');

            /** @var QuestionOption $option */
            $option = QuestionOption::find($option);

            if (!$option)
                throw new Exception('Выберите один из вариантов ответов');

            $result = $test->getResultQuestion($option->question);
            if (!$result) {
                $result = new UserResult();
            }

            $result->fill([
                'test_id' => $test->id,
                'user_id' => $this->user->id,
                'question_id' => $option->question_id,
                'options' => [$option->id],
                'is_right' => $option->is_true,
                'time' => intval($result->time) + (time() - \request('start_time', 0))
            ]);
            $result->save();

            if ($this->user->countTestResults($test) >= count($test->questions)) {
                return redirect()->route('battle.finish_test');
            }

            $nextIndex = $test->nextQuestionIndex($option->question);
            return redirect(route('battle.process')."?q=$nextIndex");
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function finishTest()
    {
        $battle = $this->user->currentBattle();
        if (!$battle) {
            return redirect()->route('battle');
        }

        if ($battle->user_id == $this->user->id){
            $battle->user_finished_at = Carbon::now();
        } else {
            $battle->opponent_finished_at = Carbon::now();
        }

        $battle->save();

        if ($battle->user_finished_at && $battle->opponent_finished_at) {
            $test = $battle->test;
            $test->finished = Carbon::now();
            $test->save();

            $battle->finished_at = Carbon::now();
            if ($battle->test->countResultRightBattle($battle->user) < $battle->test->countResultRightBattle($battle->opponent)) {
                $battle->opponent_win = 1;
            } elseif ($battle->test->countResultRightBattle($battle->user) > $battle->test->countResultRightBattle($battle->opponent)) {
                $battle->user_win = 1;
            } elseif ($battle->test->sumTimeResultRightBattle($battle->user) < $battle->test->sumTimeResultRightBattle($battle->opponent)) {
                $battle->user_win = 1;
            } else {
                $battle->opponent_win = 1;
            }
            $battle->save();
        }

        return redirect()->route('battle.result', $battle);
    }

    public function result(Battle $battle)
    {
        return view('battle.results', [
            'test' => $battle->test,
            'battle' => $battle
        ]);
    }

}
