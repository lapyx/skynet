<?php

namespace App\Http\Controllers;

use App\Models\Predmet;
use App\Models\Question;
use App\Models\User;
use App\Models\UserResult;

use DB, Log;
use App\Models\Category;

class QuestionController extends Controller
{
    /**
         * Create a new controller instance.
         *
         * @return void
     */
    public function __construct()
    {
    }

    public function float_random() {
        return (float)rand() / (float)getrandmax();
    }

    public function aliasMethod() {
        // $ar = getCategoryWeakness($predmet, $user, $count);
        $categories = $ar[0];
        $weakness = $ar[1];
        $weakness = [
            0 => 1,
            1 => 2,
            2 => 3,
            3 => 1,
            4 => 1,
        ];

        $ar_sum = array_sum($weakness);
        $ar_count = count($weakness);

        $alias = [];
        $prob = [];
        $small = [];
        $large = [];

        foreach($weakness as $key => &$one) {
            $one /= $ar_sum;
            $one *= $ar_count;
            if ($one < 1) {
                $small[] = $key;
            } else {
                $large[] = $key;
            }
        }

        while (!(empty($small) || empty($large))) {
            $l = array_shift($small);
            $g = array_shift($large);
            $prob[$l] = $weakness[$l];
            $alias[$l] = $g;
            $pg = $weakness[$g] + $weakness[$l] - 1;
            if ($pg < 1) {
                $small[] = $g;
            } else {
                $large[] = $g;
            }
        }

        while (!(empty($large))) {
            $g = array_shift($large);
            $prob[$g] = 1;
        }

        while (!(empty($small))) {
            $l = array_shift($small);
            $prob[$l] = 1;
        }

        for ($i = 0; $i < 10; $i += 1) {
            $dice = rand(0, $ar_count - 1);
            if ($this->float_random() < $prob[$dice]) {
                Log::info($dice);
            } else {
                Log::info($alias[$dice]);
            }
        }

        // Log::info($weakness);
    }
    /**
     * @param Predmet $predmet
     * @param User $user
     * @param int $count
     */
    public function rangeQuestions(Predmet $predmet, User $user, $count) {
        return $this->getCategoryWeakness($predmet, $user, $count);
    }

    /**
     * @param Predmet $predmet
     * @param User $user
     * @param int $count
     */
    public function getQuestionsList(Predmet $predmet, User $user, $count) {
        $questions = $this->rangeQuestions($predmet, $user, $count);
        if ($questions->count() >= $count) {
            $questions = $questions->limit($count)->get()->toArray();
        } elseif ($questions->count() == $count) {
            $questions->get()->toArray();
        } else {
            // добавлены рандомные
            $questions = $questions->get()->toArray();
            $addition = Question::select('questions.id')
                ->join('categories', 'category_id', 'categories.id')
                ->where('categories.predmet_id', $predmet->id)
                ->whereNotIn('questions.id', $questions)
                ->inRandomOrder()
                ->limit($count - count($questions))
            ;
            foreach ($addition->get()->toArray() as $element) {
                array_push($questions, $element);
            }
            shuffle($questions);
        }
        if (count($questions) < $count) {
            $add = Question::inRandomOrder()->limit($count - count($questions));
            foreach ($add->get()->toArray() as $element) {
                array_push($questions, $element);
            }
        }
        return $questions;
    }

    /**
     * @param Predmet $predmet
     * @param User $user
     * @param int $count
     */
    public function getCategoryWeakness(Predmet $predmet, User $user, $count) {
        $weak = UserResult::select('questions.id')
            ->join('questions', 'question_id', 'questions.id')
            ->join('categories', 'categories.id', 'questions.category_id')
            ->where('user_id', $user->id)
            ->where('categories.predmet_id', $predmet->id)
            ->where('is_right', 0)
            ->limit($count);
        return $weak;
    }
}