<?php

namespace App\Http\Controllers;

use App\Http\Controllers\QuestionController;
use App\Models\Exam;
use App\Models\Predmet;
use App\Models\QuestionOption;
use App\Models\UserResult;
use App\Models\Question;
use App\Models\User;
use App\Models\UserTest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;

class ExamController extends Controller
{
    /**
     * @param Exam $exam
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Exam $exam)
    {
        $test = $this->user->currentTest();
        if ($test) {
            return redirect()->route('exam.test', $test);
        }

        return view('exam.index', compact('exam'));
    }

    /**
     * @param Exam $exam
     * @return \Illuminate\Http\RedirectResponse
     */
    public function startTest(Exam $exam)
    {
        $test = $this->user->currentTest();
        if ($test) {
            return redirect()->route('exam.test', $test);
        }

        try {
            $predmet = $exam->findPredmet(\request('predmet_id'));
            if (!$predmet)
                throw new Exception('Неизвестный предмет');

            $ides = array_map(function($element) {
                return $element['id'];
            }, app(QuestionController::class)->getQuestionsList($predmet, $this->user, 5));
            if (!$ides)
                throw new Exception('У предмета еще не составлены вопросы');

            $test = new UserTest();
            $test->fill([
                'predmet_id' => $predmet->id,
                'user_id' => $this->user->id,
                'questions' => $ides,
            ]);
            $test->save();

            return redirect()->route('exam.test', $test);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    /**
     * @param UserTest $userTest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function test(UserTest $userTest)
    {
        if ($userTest->finished) {
            return view('exam.results', ['test' => $userTest]);
        }

        $index = \request('q', 1)-1;
        if (array_key_exists($index, $userTest->questions) === false) {
            $index = 0;
        }
        $count_questions = count($userTest->questions);
        $count_results = $this->user->countTestResults($userTest);
        $question = $userTest->getQuestion($index);
        return view('exam.test', [
            'test' => $userTest,
            'question' => $question,
            'count_questions' => $count_questions,
            'count_results' => $count_results,
            'index' => $index+1
        ]);
    }

    /**
     * @param UserTest $userTest
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setOption(UserTest $userTest)
    {
        $option = \request('option');
        try {
            if (!$option)
                throw new Exception('Выберите один из вариантов ответов');

            /** @var QuestionOption $option */
            $option = QuestionOption::find($option);

            if (!$option)
                throw new Exception('Выберите один из вариантов ответов');

            $result = $userTest->getResultQuestion($option->question);
            if (!$result) {
                $result = new UserResult();
            }

            $result->fill([
                'test_id' => $userTest->id,
                'user_id' => $this->user->id,
                'question_id' => $option->question_id,
                'options' => [$option->id],
                'is_right' => $option->is_true,
                'time' => intval($result->time) + (time() - \request('start_time', 0))
            ]);
            $result->save();

            $nextIndex = $userTest->nextQuestionIndex($option->question);
            return redirect(route('exam.test', $userTest)."?q=$nextIndex");
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function finishTest(UserTest $userTest)
    {
        $userTest->finished = Carbon::now();
        $userTest->save();

        return redirect()->route('exam.test', $userTest);
    }

}
