<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Battle
 * @package App\Models
 * @mixin Battle
 */
class Battle extends Model
{
    /**
     * Атрибуты, для которых запрещено массовое назначение.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'finished_at', 'user_finished_at', 'opponent_finished_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function opponent()
    {
        return $this->belongsTo(User::class, 'opponent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo(UserTest::class, 'test_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function predmet()
    {
        return $this->belongsTo(Predmet::class);
    }

    /**
     * @return bool
     */
    public function isProcess()
    {
        return $this->user_id && $this->opponent_id && (!$this->opponent_finished_at || !$this->user_finished_at);
    }

    /**
     * @param User $user
     * @param Predmet $predmet
     * @return Battle|null
     */
    public static function findByCriteria(User $user, Predmet $predmet)
    {
        return Battle::whereNull('opponent_id')
            ->where('predmet_id', $predmet->id)
            ->where('user_id', '!=', $user->id)
            ->whereNull('finished_at');
    }
}
