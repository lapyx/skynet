<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * тесты пол-ля
 * Class UserTest
 * @package App\Models
 * @mixin UserTest
 */
class UserTest extends Model
{
    /**
     * Атрибуты, для которых запрещено массовое назначение.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'finished_at'];

    /**
     * Атрибуты, которые нужно преобразовать в нативный тип.
     *
     * @var array
     */
    protected $casts = [
        'questions' => 'array',
    ];

    /**
     * @return Question[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getQuestions()
    {
        return Question::query()->whereIn('id', $this->questions)->get();
    }

    public function results()
    {
        return $this->hasMany(UserResult::class, 'test_id');
    }
    /**
     * @param $index
     * @return null|Question
     */
    public function getQuestion($index)
    {
        $id = array_get($this->questions, $index, 0);
        return Question::find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function predmet()
    {
        return $this->belongsTo(Predmet::class);
    }

    /**
     * @param Question $question
     * @return bool|false|int|string
     */
    public function getIndexQuestion(Question $question)
    {
        $idx = array_search($question->id, $this->questions);
        if ($idx !== false) {
            return $idx+1;
        }
        return false;
    }

    /**
     * @param Question $question
     * @return bool
     */
    public function hasNext(Question $question)
    {
        $num = $this->getIndexQuestion($question);
        $count = count($this->questions);
        return $num < $count;
    }

    /**
     * @param Question $question
     * @return bool
     */
    public function hasPrev(Question $question)
    {
        $num = $this->getIndexQuestion($question);
        return $num > 1;
    }

    /**
     * @param Question $question
     * @return bool
     */
    public function hasResultQuestion(Question $question)
    {
        return UserResult::where('test_id', $this->id)
            ->where('question_id', $question->id)
            ->where('user_id', $this->user_id)
            //->orWhere('battle_id', $this->battle_id)
            ->count() > 0;
    }

    /**
     * @param Question $question
     * @param User|null $user
     * @return null|UserResult
     */
    public function getResultQuestion(Question $question, User $user = null)
    {
        return UserResult::where('test_id', $this->id)
            ->where('question_id', $question->id)
            ->where('user_id', $user ? $user->id : $this->user_id)
            ->first();
    }

    /**
     * @return int
     */
    public function countResultRight()
    {
        return $this->user_id ? $this->results()
            ->where('is_right', true)
            //->orWhere('battle_id', $this->battle_id)
            ->count() : 0;
    }

    /**
     * @param User $user
     * @return int
     */
    public function countResultRightBattle(User $user)
    {
        return is_null($this->user_id) ? $this->results()
            ->where('user_id', $user->id)
            ->where('is_right', true)
            //->orWhere('battle_id', $this->battle_id)
            ->count() : 0;
    }

    /**
     * @param User $user
     * @return int
     */
    public function sumTimeResultRightBattle(User $user)
    {
        return is_null($this->user_id) ? $this->results()
            ->where('user_id', $user->id)
            ->where('is_right', true)
            //->orWhere('battle_id', $this->battle_id)
            ->sum('time') : 0;
    }

    /**
     * @param Question $question
     * @param QuestionOption $option
     * @return bool
     */
    public function checkOptionResult(Question $question, QuestionOption $option)
    {
        $result = $this->results()->where('question_id', $question->id)->first();
        return $result && in_array($option->id, $result->options);
    }

    /**
     * @param Question $question
     * @return bool|false|int|string
     */
    public function nextQuestionIndex(Question $question)
    {
        $currentIndex = $this->getIndexQuestion($question);
        $nextQuestion = $this->getQuestion($currentIndex);
        if ($nextQuestion) {
            return $this->getIndexQuestion($nextQuestion);
        }
        return $currentIndex;
    }

    /**
     * @return bool
     */
    public function isPassed()
    {
        return $this->countResultRight()/count($this->questions) > 0.6;
    }
}
