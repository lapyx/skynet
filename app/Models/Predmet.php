<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Predmet
 * @mixin Predmet
 * @package App\Models
 */
class Predmet extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }

    /**
     * @return Question[]
     */
    public function getQuestions()
    {
        $ids = $this->categories->pluck('id', 'id')->toArray();
        if ($ids) {
            return Question::query()->whereIn('category_id', $ids)->get();
        }
        return [];
    }
}
