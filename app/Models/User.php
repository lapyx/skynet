<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 * @mixin User
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(UserResult::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tests()
    {
        return $this->hasMany(UserTest::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function currentTest()
    {
        return $this->tests()->whereNull('finished')->first();
    }

    public function battles()
    {
        $user_id = $this->id;
        return Battle::where(function($query) use ($user_id) {
            $query->where('user_id', $user_id);
            $query->orWhere('opponent_id', $user_id);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null|Battle
     */
    public function currentBattle()
    {
        return $this->battles()
            ->where(function($query){
                $query->whereNull('user_finished_at')
                    ->orWhereNull('opponent_finished_at');
            })->first();
    }

    /**
     * @param UserTest $test
     * @return int
     */
    public function countTestResults(UserTest $test)
    {
        return $this->results()->where('test_id', $test->id)->count();
    }

    /**
     * @return bool
     */
    public function inBattle()
    {
        return isset($this->currentBattle()->id);
    }

    /**
     * @return string
     */
    public function link()
    {
        return '<a class="btn-link" href="'.route('profile', $this).'">'.$this->name.'</a>';
    }
}
