<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function predmets()
    {
        return $this->hasMany(Predmet::class);
    }

    /**
     * @param $id
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null|Predmet
     */
    public function findPredmet($id)
    {
        return $this->predmets()->where('id', $id)->first();
    }

}
