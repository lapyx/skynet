<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class SkynetProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
        });

        Schema::create('predmets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('exam_predmets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_id');
            $table->foreign('exam_id')->references('id')->on('exams');
            $table->unsignedInteger('predmet_id');
            $table->unique(['exam_id', 'predmet_id'], 'exam_predmet_unique');
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('predmet_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedInteger('exam_predmet_id');
            $table->unique(['category_id', 'exam_predmet_id'], 'category_exam_predmet_unique');
            $table->foreign('exam_predmet_id')->references('id')->on('exam_predmets');
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('predmet_category_id');
            $table->foreign('predmet_category_id')->references('id')->on('predmet_categories');
            $table->string('name', 3000);
        });

        Schema::create('question_options', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_id');
            $table->foreign('question_id')->references('id')->on('questions');
            $table->string('option', 512);
            $table->boolean('is_true')->default(false);
            $table->timestamps();
        });

        Schema::create('group_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('questions');
            $table->timestamps();
            $table->dateTime('finished');
        });

        Schema::create('user_results', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('group_id');
            $table->foreign('group_id')->references('id')->on('group_questions');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedInteger('question_id');
            $table->foreign('question_id')->references('id')->on('questions');

            $table->string('options')->default('[]');
            $table->boolean('is_right');
            $table->timestamp('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
