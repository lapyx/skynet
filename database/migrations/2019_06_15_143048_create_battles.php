<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_id');
            $table->unsignedInteger('predmet_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('opponent_id')->nullable();
            $table->unsignedInteger('group_id')->nullable();

            $table->dateTime('finished_at')->nullable();
            $table->dateTime('user_finished_at')->nullable();
            $table->dateTime('opponent_finished_at')->nullable();

            $table->boolean('user_win')->default(0);
            $table->boolean('opponent_win')->default(0);

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('opponent_id')->references('id')->on('users');
            $table->foreign('group_id')->references('id')->on('group_questions');
            $table->foreign('exam_id')->references('id')->on('exams');
            $table->foreign('predmet_id')->references('id')->on('predmets');

            $table->timestamps();
        });

        Schema::table('user_results', function (Blueprint $table) {
            $table->unsignedInteger('battle_id')->nullable()->after('question_id');
            $table->foreign('battle_id')->references('id')->on('battles');
        });

        Schema::create('battle_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('battle_id');
            $table->string('categories')->default('[]'); // предложенные категории
            $table->string('chek_categories')->default('[]'); // выбранные категории
            $table->unsignedInteger('last_user_id')->nullable(); // последний убравший категорию

            $table->foreign('battle_id')->references('id')->on('battles');
            $table->foreign('last_user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_categories');
        Schema::table('user_results', function (Blueprint $table) {
            $table->dropColumn('battle_id');
        });
        Schema::dropIfExists('battles');
    }
}
