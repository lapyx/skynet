<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\BattleCategory
 *
 * @property int $id
 * @property int $battle_id
 * @property array $categories
 * @property array $chek_categories
 * @property int|null $last_user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Battle $battle
 * @property-read \App\Models\User|null $lastUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereBattleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereChekCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereLastUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BattleCategory whereUpdatedAt($value)
 */
	class BattleCategory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Question
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QuestionOption[] $options
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Question whereName($value)
 */
	class Question extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Exam
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Predmet[] $predmets
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Exam whereTitle($value)
 */
	class Exam extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserResult[] $results
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserTest[] $tests
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * тесты пол-ля
 * Class UserTest
 *
 * @package App\Models
 * @mixin UserTest
 * @property int $id
 * @property int $predmet_id
 * @property int|null $user_id
 * @property array $questions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $finished
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest wherePredmetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserTest whereUserId($value)
 */
	class UserTest extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserResult
 *
 * @property int $id
 * @property int $test_id
 * @property int $user_id
 * @property int $question_id
 * @property int|null $battle_id
 * @property array $options
 * @property int $is_right
 * @property string $time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Question $question
 * @property-read \App\Models\UserTest $test
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereBattleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereIsRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserResult whereUserId($value)
 */
	class UserResult extends \Eloquent {}
}

namespace App\Models{
/**
 * Class Predmet
 *
 * @mixin Predmet
 * @package App\Models
 * @property int $id
 * @property int $exam_id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Predmet whereUpdatedAt($value)
 */
	class Predmet extends \Eloquent {}
}

namespace App\Models{
/**
 * Class QuestionOption
 *
 * @package App\Models
 * @mixin QuestionOption
 * @property int $id
 * @property int $question_id
 * @property string $option
 * @property int $is_true
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereIsTrue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\QuestionOption whereUpdatedAt($value)
 */
	class QuestionOption extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Battle
 *
 * @property int $id
 * @property int $exam_id
 * @property int $predmet_id
 * @property int $user_id
 * @property int|null $opponent_id
 * @property int|null $test_id
 * @property \Illuminate\Support\Carbon|null $finished_at
 * @property \Illuminate\Support\Carbon|null $user_finished_at
 * @property \Illuminate\Support\Carbon|null $opponent_finished_at
 * @property int $user_win
 * @property int $opponent_win
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Exam $exam
 * @property-read \App\Models\User|null $opponent
 * @property-read \App\Models\Predmet $predmet
 * @property-read \App\Models\UserTest $test
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereOpponentFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereOpponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereOpponentWin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle wherePredmetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereUserFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Battle whereUserWin($value)
 */
	class Battle extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property int $exam_id
 * @property int $predmet_id
 * @property string $name
 * @property-read \App\Models\Question $questions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereExamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category wherePredmetId($value)
 */
	class Category extends \Eloquent {}
}

