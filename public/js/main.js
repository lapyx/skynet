/* Popup Flash message begin */
function popupError(message, timesec) {
    popup(message, timesec, 'danger');
}

function popup(message, timesec, type) {
    type = type || 'success';
    var flash = $('#flashPopup');
    timesec = timesec ? timesec+'000' : 5000;
    if (!flash.length) {
        flash = $('<div id="flashPopup" class="flash-container flash-container-desktop ng-scope">');
        $('body').append(flash);
    }
    if (flash.find('.flash-message').length >= 10) {
        flash.find('.flash-message').first().remove();
    }
    var flashMessage = $('<div class="flash-message alert ng-binding ng-scope alert-'+type+'">');
    flashMessage.append('<button type="button" class="close" data-dismiss="alert">&times;</button>');
    flashMessage.append(message);
    flash.append(flashMessage);
    setTimeout(function() { flashMessage.remove(); }, timesec);
}
/* Popup Flash message end */